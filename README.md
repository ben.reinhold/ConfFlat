# Conformal Flattening
[![Documentation Status](https://readthedocs.org/projects/conformal-flattening/badge/?version=latest)](https://conformal-flattening.readthedocs.io/en/latest/?badge=latest)  

This project implements and visualizes the "boundary first flattening" algorithm presented in the [same-titled paper](https://arxiv.org/abs/1704.06873) by Rohan Sawhney and Keenan Crane in Python. It was created as part of the course "Advanced practical course in scientific computing" at the University of G&ouml;ttingen under the advision of Dr. Jochen Schulz and Prof. Max Wardetzky. 

You can find its documentation together with an outline of the algorithm [here](https://conformal-flattening.readthedocs.io/en/latest/).

## Table of contents

- [Installation](#installatio)
	- [Linux](#linux)
	- [Windows](#windows)
	- [MacOS](#macos)
- [Documentation](#documentation)
- [Samples](#samples)
- [Useful Resources](#useful-resources)

# Installation 
The dependencies of `bff` are `numpy`, `cvxopt`, `mayavi` and `matplotlib`.  Due to the large dependencies of `mayavi`, which is used for 3d plotting, we recommend to install these packages in a seperate enviroment. Below, you find detailed installation instructions that only assume an existing installation of the package manager `conda` (see e.g. [Miniconda](https://docs.conda.io/en/latest/miniconda.html)).

## Linux

First, we create an enviroment `env_name` using `conda`.

```console
$ conda create -n env_name
```

To activate, deactivate or remove this enviroment, you can use the following commands:

```console
$ conda activate env_name
$ conda deactivate
$ conda env remove -n env_name
```

You can then install the needed packages into the enviroment using:

```console
$ conda install -n env_name numpy cvxopt mayavi matplotlib
```

You can then activate you enviroment (see above) and start your favourite python IDE/editor. 

As a starting point, you can check out the page [First Steps](https://conformal-flattening.readthedocs.io/en/latest/firststeps.html) in the documentation or the `examples.ipynb` jupyter notebook for a more interactive overview of the module functions. For the latter, first install jupyter:
```console
$ conda install -n env_name jupyter
```
And then launch the Jupyter HTML Notebook (after activating `env_name`):
```console
$ jupyter notebook
```

## Windows

First, we create an enviroment `env_name` using `conda`:

```console
$ conda create -n env_name
```

To activate, deactivate or remove this enviroment, you can use the following commands:

```console
$ activate env_name
$ conda deactivate
$ conda env remove -n env_name
```

You can then install the needed packages into the enviroment using:

```console
$ conda install -n env_name numpy cvxopt mayavi matplotlib
```

You can then activate you enviroment (see above) and start your favourite python IDE/editor. 

---
**Note:**
Due to a [bug](https://stackoverflow.com/a/53889608), you might have to also run (after activating you enviroment):

```console
$ pip install -U apptools
```
To check if your `mayavi` installation is working, run:

```console
$ mayavi2
```
---

As a starting point, you can check out the page [First Steps](https://conformal-flattening.readthedocs.io/en/latest/firststeps.html) in the documentation or the `examples.ipynb` jupyter notebook for a more interactive overview of the module functions. For the latter, first install jupyter:
```console
$ conda install -n env_name jupyter
```
And then launch the Jupyter HTML Notebook (after activating `env_name`):
```console
$ jupyter notebook
```

## MacOS

First, we create an enviroment `env_name` using `conda`.

```console
$ conda create -n env_name
```

To activate, deactivate or remove this enviroment, you can use the following commands:

```console
$ conda activate env_name
$ conda deactivate
$ conda env remove -n env_name
```

You can then install the needed packages into the enviroment using:

```console
$ conda activate env_name
$ conda install numpy cvxopt mayavi matplotlib
$ pip install pyqt5
```

You can then activate you enviroment (see above) and start your favourite python IDE/editor. 

As a starting point, you can check out the page [First Steps](https://conformal-flattening.readthedocs.io/en/latest/firststeps.html) in the documentation or the `examples.ipynb` jupyter notebook for a more interactive overview of the module functions. For the latter, first install jupyter:
```console
$ conda install -n env_name jupyter
```
And then launch the Jupyter HTML Notebook (after activating `env_name`):
```console
$ jupyter notebook
```

# Documentation
You can find the documentation of `bff` alongside some mathematical background [hosted on Read the Docs](https://conformal-flattening.readthedocs.io/en/latest/).

To generate the documentation locally, first install `sphinx` and its extensions `numpydoc` and `sphinxcontrib-bibtex` into `env_name` (after installing the modules [above](#installation)):
```console
$ conda activate env_name
$ pip install sphinx sphinxcontrib-bibtex numpydoc
```

And run in `docs/`:

```
$ make html
```
The documentation will then be build in `docs/_build/html/`.

# Samples
You can find a few sample meshes in form of `.ply` and `.obj` files in the directory `Samples/`. These were taken from [here](https://people.sc.fsu.edu/~jburkardt/data/ply/ply.html) and [here](https://github.com/GeometryCollective/boundary-first-flattening/tree/master/input).

# Useful Resources
[reStrucutredText cheat sheet](http://docutils.sourceforge.net/docs/user/rst/quickref.html)  
[Markdown cheat sheet](https://www.markdownguide.org/basic-syntax/)  
[numpydoc styleguide](https://numpydoc.readthedocs.io/en/latest/)  
[PEP 8 -- Style Guide for Python Code](https://www.python.org/dev/peps/pep-0008/)  