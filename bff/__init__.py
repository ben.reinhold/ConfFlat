"""
The ``bff`` module implements the boundary first flattening algorithm 
from a `paper <https://arxiv.org/abs/1704.06873>`__ by Rohan Sawhney 
and Keenan Crane. You can find its GitLab repository `here <https://
gitlab.gwdg.de/ben.reinhold/ConfFlat>`_ and its documentation `here 
<https://conformal-flattening.readthedocs.io/en/latest/index.html>`__.
"""

import numpy as np
import matplotlib.pyplot as plt

from cvxopt import matrix, spmatrix
from cvxopt.cholmod import solve, symbolic, numeric
from csv import reader
from itertools import takewhile
from mayavi.mlab import triangular_mesh, show
from itertools import chain


__all__ = ['best_fit_curve',
           'load_mesh',
           'TriMesh',
           'FlatMesh',
           'example_curve'
           ]


#===============================Classes========================================

class TriMesh:
    """Class to process triangle meshes with the boundary first 
    flattening algorithm
    
    
    Parameters
    ----------
    v : list of tuple of float
        List of vertices; elements are tuples containing the 
        vertex coordinates
    f : list of tuple of int
        List of faces; elements are tuples containing three 
        vertex indices of vertices constituting a face. 
        Vertex indices start at 0.
        
    Attributes
    ----------
    v : list of tuple of float
        List of vertices; elements are tuples containing the
        vertex coordinates
    f : list of tuple of int
        List of faces; elements are tuples containing three
        vertex indices of vertices constituting a face.
        Vertex indices start at 0
    v_num : int
        Number of vertices
    f_num : int
        Number of faces
    b_num : int
        Number of boundary vertices
    i_num : int
        Number of interior vertices (`i_num` = `v_num` - `b_num`)
    int_ang : list of list of float
        List of interior angles; n-th entry is a list [beta1, beta2, 
        beta3] containing the interior angles of the n-th face 
        (with beta1 being the angle at the first vertex, etc.)
    cmap : str
        Colormap for plotting the triangle mesh. Defaults to 'viridis'.
    scalars : str or list
        Scalar data when plotting the mesh. Vertices will be colored
        with respect to these data and the colormap `cmap`. 
        Can be either a list containing a scalar value for each vertex
        or an element of ['x', 'y', 'z']. If it's one of the latter, 
        the corresponding coordinate values will be used. 
        Defaults to 'z'.
        
    Notes
    -----
    Due to high costs, the attributes `b_num`, `i_num` and `int_ang` 
    are not computed on initialization. The attribute `int_ang` is 
    computed by methods on demand, while `b_num` and `i_num` are 
    written when executing :any:`reorder`. To compute `int_ang` 
    manually, you can use :any:`_find_int_ang`.
    """
    
    
    def __init__(self, v, f):
        
        # Safe vertices and faces as attributes
        self.v = v
        self.f = f
        
        # Compute the vertex/face numbers and write them to attributes
        self.v_num = len(v)
        self.f_num = len(f)
        
        # Set default colormap and scalars for plotting the mesh
        self.scalars = 'z'
        self.cmap = 'viridis'
        
        # Set private attribute to check if the vertices were ordered
        self._sorted = False
        
        
    def reorder(self):
        """Finds the boundary and reorders the vertices; has to be 
        used before :any:`flatten`
        
        If not done previously, finds the boundary of the triangle
        mesh and reorders the vertices such that:
            
            - The indices of the interior vertices come first
            - the boundary vertices are ordered such that consecutive 
              vertices are connected by a boundary edge
            
        The attributes `b_num` and `i_num` are written in the process.
        
        Notes
        -----
        - If the vertices were previously reordered by this method,
          this method does nothing
        - A dictionary translating the old vertex indices to the new 
          ones can be found after execution under the attribute `_vdic`
        - If the mesh has been reordered before, using :any:`reorder` 
          or :any:`_bound_sort`, it will not be reordered again
        - If the `scalar` attribute of the TriMesh object is a list
          containing scalar values for each vertex, it will be
          reordered accordingly
        - The relative order of the non-boundary vertices will be
          preserved
          
        Raises
        ------
        AssertionError
            If no boundary was found or is not connected
            
        Examples
        --------
        >>> from bff import load_mesh
        >>> from numpy import pi
        >>> mesh = load_mesh('Samples/face.obj')
        >>> mesh.reorder()
        >>> k = [2*pi/mesh.b_num] * mesh.b_num
        >>> flat = mesh.flatten(k)
        
        >>> from bff import load_mesh
        >>> face = load_mesh('Samples/face.obj')
        >>> face.reorder()
        >>> face._vdic[200]
        190
        """
        
        if self._sorted == False:    
            self._bound_sort()
        
 
    def build_laplacian(self):
        """Builds the discrete Laplacian of the triangle mesh
        
        Returns the discrete Laplacian of the triangle mesh which is 
        computed via the "cotan formula". Writes the attribute 
        `int_ang` if it is not already present.
        
        Returns
        -------
        array_like
            Discrete Laplacian as a cvxopt.base.spmatrix object. 
            See the `CVXOPT User's Guide <http://cvxopt.org/userguide
            /matrices.html#sparse-matrices>`_ for more information
            
        Examples
        --------
        >>> from bff import load_mesh
        >>> mesh = load_mesh('Samples/face.obj')
        >>> mesh.build_laplacian()
        <17157x17157 sparse matrix, tc='d', nnz=119757>
        """
        
        # Initiate empty lists for matrix indices and values 
        val = []
        x = []
        y = []
        
        # Check if the int_ang was already computed
        try:
            # Compute 0.5*cotan(beta) for the interior angles
            C = 1/(2*np.tan(np.array(self.int_ang)))
        except AttributeError:
            
            # Compute interior angles 
            self._find_int_ang()
            
            # Compute 0.5*cotan(beta) for the interior angles
            C = 1/(2*np.tan(np.array(self.int_ang)))
        
        # Fill the lists
        for (v1, v2, v3), (C1, C2, C3) in zip(self.f, C.tolist()):
                   
            # Fill the lists
            val.extend([C3, C3, -C3, -C3])
            x.extend([v1, v2, v1, v2])
            y.extend([v1, v2, v2, v1])
            
            val.extend([C1, C1, -C1, -C1])
            x.extend([v2, v3, v2, v3])
            y.extend([v2, v3, v3, v2])
            
            val.extend([C2, C2, -C2, -C2])
            x.extend([v3, v1, v3, v1])
            y.extend([v3, v1, v1, v3])
        
        # Add a small identity matrix for a more stable cholesky
        # factorization
        x.extend(range(self.v_num))
        y.extend(range(self.v_num))
        val.extend([1e-8]*self.v_num)
                
        # Build and return sparse matrix
        return spmatrix(val, x, y)

       
    def discrete_curvature(self):
        """        
        Computes the discrete gaussian curvature at interior and the 
        discrete geodesic curvature at boundary vertices.
        
        Assumes that the vertices have been reordered using the 
        method :any:`reorder` before. This is needed to distinguish
        between interior and exterior vertices.
        
        If not done previously, the attribute `int_ang` will be 
        written.
        
        Returns
        -------
        list
            List where the n-th entry is the discrete gaussian/
            geodesic curvature at the n-th vertex of the mesh
            
        Raises
        ------
        AssertionError
            If the vertices were not reordered by the method
            :any`reorder` before
        
        Examples
        --------
        >>> from bff import load_mesh
        >>> mesh = load_mesh('Samples/face.obj')
        >>> mesh.reorder()
        >>> C = mesh.discrete_curvature()
        """
        
        # Check if the method `reorder` has been used
        try:
            assert self._sorted, 'Vertices not found'
        except AssertionError:
            print('Please reorder the vertices first using the'
                  + 'method `reorder`')
            raise        
        
        # Initiate list for discrete curvature
        curv = [2*np.pi]*self.i_num + [np.pi]*self.b_num
  
        # Check for int_ang attribute
        try:
            self.int_ang
        except AttributeError:
            self._find_int_ang()
        
        # Fill array
        for (v1, v2, v3), (a1, a2, a3) in zip(self.f, self.int_ang):
        
            # Subtract interior angle from the corresponding vertex 
            # entry in curv
            curv[v1] -= a1
            curv[v2] -= a2
            curv[v3] -= a3
            
        return curv
    

    def bound_edge_len(self):
        """Computes boundary edge lengths
        
        Returns List of boundary edge lengths in the 
        form [b1b2, b2b3, etc.] where bi is the i-th boundary vertex.
        Assumes that the boundary has been found before by usage of
        :any:`reorder`.
        
        Returns
        -------
        list
            List of boundary lengths
            
        Examples
        --------
        >>> from bff import load_mesh
        >>> mesh = load_mesh('Samples/face.obj')
        >>> mesh.reorder()
        >>> lengths = mesh.bound_edge_len()
        
        Raises
        ------
        AssertionError
            If the boundary has not been found before using the
            method :any:`reorder`
        """
        
        # Check if the method `reorder` has been used
        try:
            assert self._sorted, 'Vertices not found'
        except AssertionError:
            print('Please reorder the vertices first using the'
                  + 'method `reorder`')
            raise      
            
        # Get array of boundary vertices
        bv = np.array(self.v[self.i_num:])
        
        # Cyclic shift by -1 and compute boundary edge lengths
        bv2 = np.roll(bv, -1, axis=0)
        return np.linalg.norm(bv-bv2, axis=1)


    def cut(self, i, depth, cut_out = False):
        """Removes all vertices in a given range around a given vertex
        
        Finds all vertices connected to the `i`-th vertex by following
        a maximum of `depth` edges. These are then by default removed 
        from the triangle mesh. If `cut_out` is True, instead the 
        submesh spanned by these vertices is returned.
        
        Parameters
        ----------
        i : int
            Index of vertex the cut is centered at
        depth : int
            Range of the cut around `i`
        cut_out : bool, optional
            If `False`, all vertexes in a range of `depth` around the 
            `i`-th vertex are removed. If `True`, only these are kept. 
            Defaults to `False`.
        
        Returns
        -------
        TriMesh object
            Cut mesh
                    
        See Also
        --------
        cut_out : Calls :any:`cut` with keyword argument `cut_out 
            = True` for convenience 
        
        Examples
        --------
        >>> from bff import load_mesh
        >>> cow = load_mesh('Samples/cow.ply')
        >>> cow_with_hole = cow.cut(740, 5)
        
        >>> from bff import load_mesh
        >>> bunny = bff.load_mesh('Samples/bunny.obj')
        >>> bunny_head = bunny.cut(1, 50, cut_out=True)
        """
        
        # Find the neighbors of all vertices
        nb = [set() for _ in range(self.v_num)]
        
        for face in self.f:
            a, b, c = face
            
            nb[a].add(b)
            nb[a].add(c)
            
            nb[b].add(a)
            nb[b].add(c)
            
            nb[c].add(a)
            nb[c].add(b)
        
        # Set to gather vertices connected to the `i`-th one by a 
        # maximum of `depth` edges
        rm = set([i])
        
        # Set that witnesses which vertices were added in the last of 
        # the `depth` iterations
        rm_new = rm.copy()
        
        # `depth`-times: find the neighbors of vertices in `rm_new` and 
        # add them to `rm_new`
        for _ in range(depth):
            for j in rm_new.copy():
                rm_new |= nb[j]
            
            # Find vertices added this iteration step
            rm_new -= rm
            
            # Add new vertices to `rm`
            rm |= rm_new
        
        # Initiate empty list for new vertices
        new_v = []
        
        # Inititate dictionary to translate old vertex indices to 
        # new ones
        dic = {}
        
        # Check value of `cut_out` and choose accordingly which
        # vertices to keep. Then fill the list and dictionary 
        # accordingly
        if cut_out == False:
            
            kept_ind = set(range(self.v_num)) - rm
            
            #Check if there are any indices left
            if not kept_ind:
                print('Warning: `cut` returned empty triangle'
                      + ' mesh')
                return TriMesh([], [])
            
            for new_ind, old_ind in enumerate(kept_ind):
                
                new_v.append(self.v[old_ind])
                dic[old_ind] = new_ind
            
        elif cut_out == True:
            
            for new_ind, old_ind in enumerate(rm):
                
                new_v.append(self.v[old_ind])
                dic[old_ind] = new_ind
        
        # Initiate empty list vor new `f` attribute
        new_f = []
        
        # Fill list depending on the `cut_out` value
        if cut_out == False:
            for f1, f2, f3 in self.f:
                if f1 not in rm and f2 not in rm and f3 not in rm:
                    new_f.append((dic[f1], dic[f2], dic[f3]))
                    
        elif cut_out == True:
            for f1, f2, f3 in self.f:
                if f1 in rm and f2 in rm and f3 in rm:
                    new_f.append((dic[f1], dic[f2], dic[f3]))                  
        
        return TriMesh(new_v, new_f)
        
    
    def cut_out(self, i, depth):
        """Cuts out a submesh spanned by all vertices in a given range
        around a given vertex
        
        Cuts out the submesh spanned by all vertices connected the 
        `i`-th vertex by a maximum of `depth` edges.
        Calls the method :any:`cut` with `cut_out = True`. See its 
        documentation for more information.
        
        Examples
        --------
        >>> from bff import load_mesh
        >>> bunny = bff.load_mesh('Samples/bunny.obj')
        >>> bunny_head = bunny.cut_out(1, 50)
        """
        return self.cut(i, depth, cut_out = True)


    def plot(self):
        """Plots mesh in 3d
        
        Plots a triangle mesh in 3d using `mayavi.mlab.
        triangular_mesh`.
        
        Notes
        -----
        To change the colormap or the scalar data used to plot the 
        mesh, change the according attributes of the :any:`TriMesh`
        object.
        
        Examples
        --------
        >>> from bff import load_mesh
        >>> face = load_mesh('Samples/face.obj')
        >>> face.plot()
        
        >>> from bff import load_mesh
        >>> bunny = bff.load_mesh('Samples/bunny.obj')
        >>> bunny_head = bunny.cut_out(1, 50)
        >>> bunny_head.plot()
        """
        
        # Get vertex data
        x = [vertex[0] for vertex in self.v]
        y = [vertex[1] for vertex in self.v]
        z = [vertex[2] for vertex in self.v]
        
        # Determine `scalars` value for plotting
        if type(self.scalars) != str:
            scalars = self.scalars
        
        elif self.scalars == 'z':
            scalars = z
            
        elif self.scalars == 'x':
            scalars = x
            
        elif self.scalars == 'y':
            scalars = y
            
        # Plot
        triangular_mesh(x, y, z, self.f, colormap=self.cmap, scalars=scalars)
        show()


    def euler_char(self):
        """
        Computes the Euler characteristic of the triangle mesh
        
        Computes the Euler characteristic of the triangle mesh using 
        the formula 
        
        .. math::
            \chi = V - E + F.
            
        Returns
        -------
        int
            Euler characteristic of the triangle mesh
            
        Examples
        --------
        >>> from bff import load_mesh
        >>> face = load_mesh('Samples/face.obj')
        >>> face.euler_char()
        1
        
        >>> from bff import load_mesh
        >>> bunny = bff.load_mesh('Samples/bunny.obj')
        >>> bunny.euler_char()
        2
        >>> bunny_head = bunny.cut_out(1, 50)
        >>> bunny_head.euler_char()
        1
        """
        
        # Initiate set for edges
        edges = set()
                
        # Iterate through faces and find edges
        for f1, f2, f3 in self.f:
            
            edges.add(frozenset([f1, f2]))
            edges.add(frozenset([f2, f3]))
            edges.add(frozenset([f3, f1]))
        
        # Get edge number and compute the Euler characteristic
        return self.v_num - len(edges) + self.f_num
    
    
    def flatten(self, k, scale_factors=False, holomorphic=True):
        """Flatten with respect to prescribed exterior angles or 
        scale factors
        
        Flattens the triangle mesh using the "Boundary First 
        Flattening" presented in the `same-titled paper 
        <https://arxiv.org/abs/1704.06873>`_.
        You can find details in the `documentation <https://conformal
        -flattening.readthedocs.io/en/latest/math.html>`_.
        
        Parameters
        ----------
        k : list
            Target exterior angles or scale factors at the 
            boundary vertices
        scale_factors : bool, optional
            If `True`, `k` is interpreted as scale factors. If `False`,
            `k` is interpreted as exterior angles. Defaults to `False`.
        holomorphic : bool, optional
            If `True`, a harmonic conjugation will be used when 
            extending the boundary curve to the whole mesh. If
            `False`, a harmonic extension will be used instead.
            The latter precisely realises the exterior angles `k`. 
            See [1]_, Section 4.4 for details. Defaults to `True`.
            
        Returns
        ------
        FlatMesh object
            Flattened mesh
        
        Raises
        ------
        AssertionError
            If vertices were not reordered before using :any:`reorder`
        
        See Also
        --------
        uniformize : Flattens mesh to disk
        flatten_to_curve : Flattens mesh to shape determined by 
            given boundary curve
        
        Examples
        --------
        >>> from bff import load_mesh
        >>> from numpy import pi
        >>> mesh = load_mesh('Samples/face.obj')
        >>> mesh.reorder()
        >>> k = [2*pi/mesh.b_num] * mesh.b_num
        >>> flat = mesh.flatten(k)
        
        >>> from bff import load_mesh
        >>> mesh = load_mesh('Samples/face.obj')
        >>> mesh.reorder()
        >>> u = [1] * mesh.b_num
        >>> flat = mesh.flatten(u,scale_factors=True)
        
        References
        ----------
        .. [1] `Sawhney, Rohan, and Keenan Crane. “Boundary First 
           Flattening.” ACM Transactions on Graphics 37.1 (2017): 
           1–14. Crossref. Web. <https://arxiv.org/abs/1704.06873>`_
        """
        
        try:
            assert self._sorted == True, 'Vertices not ordered'
        except:
            
            # Generate warning message
            if scale_factors:
                s = 'scale factors'
            else:
                s = 'exterior angles'
                
            print('Can\'t assign ' + s + ' to boundary vertices'
                  + ' unambiguously if vertices are not ordered. '
                  + 'Please reorder vertices first using the '
                  + 'method `reorder`.')
            raise 
        
        # Compute the discrete laplacian and the discrete curvature
        # int_ang attribute will be computed if wasn't previously
        A = self.build_laplacian()
        C = self.discrete_curvature()
        
        # Convert k and C into cvxopt.matrix objects
        k = matrix(k)
        C = matrix(C)
    
        # Compute the sparse cholseky factorization of A and one of 
        # its submatrices
        L = symbolic(A)
        numeric(A, L)
    
        A_II = A[:self.i_num, :self.i_num]
        L_II = symbolic(A_II)
        numeric(A_II, L_II)
    
        # Compute complementary data
        if scale_factors == False:
            u = _ext_ang_to_scale(L, C, k)
        
        elif scale_factors == True:
            u = k
            k = _scale_to_ext_ang(A, L_II, C, u)
        
        # Convert u to ndarray
        u = np.array(u).reshape(self.b_num)
        
        # Find the lengths of the boundary edges
        el = self.bound_edge_len()
        
        # Compute the target edge lengths using the scale factor so
        # that el_new[i] = np.exp((u[i] + u[i + 1])/2) * el[i]
        el_new = np.exp((u + np.roll(u, -1))/2) * el
     
        # Find a closed curve that fits the given data as best as 
        # possible
        gt = best_fit_curve(el, el_new, k)
        
        # Extend the boundary curve holomorphically to the whole 
        # triangle mesh
        x, y = _extend_curve(self, A, L, L_II, gt, holomorphic=holomorphic)
        
        return FlatMesh(x, y, self)
 
    
    def flatten_to_curve(self, curve, length, it=20):
        """Flatten to a shape determined by a boundary curve
        
        Flattens the triangle mesh using the "Boundary First 
        Flattening" algorithm to a shape determined by a prescribed 
        oriented boundary curve in arc-length parametrization.
        See [1]_, Section 6.6 for details.
        
        Parameters
        ----------
        curve : function
            Python function mapping floats in closed interval 
            [0, `length`] to complex numbers. The curve
            described by this function has to be oriented,
            closed and in arc-length parametrization
        length : float
            Length of the boundary curve
        it : int, optional
            Number of iterations to find the most fitting 
            exterior angles to use for Boundary First Flattening.
            Defaults to 20
            
        Returns
        -------
        FlatMesh object
            Flattened mesh
        
        Notes
        -----
        - If vertices were not reordered before using the method
          :any:`reorder`, this will be done in the process.
        - Bear in mind that the curve has to be closed, oriented 
          and in arc-length parametrization
        
        See Also
        --------
        flatten : Boundary First Flattening for specified exterior
            angles or scale factors
        uniformize : Flattens mesh to disk
        
        Examples
        --------
        >>> from bff import load_mesh, example_curve
        >>> mesh = load_mesh('Samples/face.obj')
        >>> flat = mesh.flatten_to_curve(example_curve, 6)
        
        >>> from bff import load_mesh
        >>> bunny = load_mesh('Samples/bunny.obj')
        >>> head = bunny.cut_out(1, 50)
        >>> spiky_head = head.flatten_to_curve(example_curve, 6, 40)
        
        References
        ----------
        .. [1] `Sawhney, Rohan, and Keenan Crane. “Boundary First 
           Flattening.” ACM Transactions on Graphics 37.1 (2017): 
           1–14. Crossref. Web. <https://arxiv.org/abs/1704.06873>`_
        """
        
        # Ensure that vertices are reordered
        self.reorder()
        
        # Compute the discrete laplacian and the discrete curvature
        # int_ang attribute will be computed if wasn't previously
        A = self.build_laplacian()
        C = matrix(self.discrete_curvature())
        
        n = self.b_num
    
        # Compute the sparse cholseky factorization of A and one of 
        # its submatrices
        L = symbolic(A)
        numeric(A, L)
    
        A_II = A[:self.i_num, :self.i_num]
        L_II = symbolic(A_II)
        numeric(A_II, L_II)
        
        # Find the lengths of the boundary edges
        el = self.bound_edge_len()
        
        # Find initial exterior angles
        s = np.empty(n)
        si = 0
        for i in range(n):
            s[i] = si
            si += el[i]
        
        s = (length/si)*s
        z = np.array([curve(x) for x in s])
        zn = np.roll(z, -1) - z
        zd = z - np.roll(z, 1)
        
        k = matrix(np.angle(zn/zd))
        
        # Correct exterior angles using an iteration process
        for _ in range(it):
            
            # Find complementary boundary data for current value of `k`
            u = _ext_ang_to_scale(L, C, k)
            u = np.array(u).reshape(self.b_num)

            # Compute the target edge lengths using the scale factor so
            # that el_new[i] = np.exp((u[i] + u[i + 1])/2) * el[i]
            el_new = np.exp((u + np.roll(u, -1))/2) * el
            
            # Sample curve w.r.t. new edge lengths
            s = np.empty(n)
            si = 0
            for i in range(n):
                s[i] = si
                si += el_new[i]

            # Sample curve and compute new `k`
            s = (length/si)*s
            z = np.array([curve(x) for x in s])
            zn = np.roll(z, -1) - z
            zd = z - np.roll(z, 1)
            
            k = matrix(np.angle(zn/zd))

        # Compute `u` and `el_new` for a final time
        u = _ext_ang_to_scale(L, C, k)
        u = np.array(u).reshape(self.b_num)
        
        el_new = np.exp((u + np.roll(u, -1))/2) * el
        
        # Compute the boundary curve using the iterated k
        gt = best_fit_curve(el, el_new, k)
        
        # Extend the boundary curve holomorphically to the whole 
        # triangle mesh
        x, y = _extend_curve(self, A, L, L_II, gt, holomorphic=False)
        
        return FlatMesh(x, y, self)


    def uniformize(self, it=10):
        """Flattens mesh to disk
        
        Uses an iteration process to find target exterior angles 
        for the Boundary First Flattening algorithm to flatten the
        triangle mesh to a disk. Also carries out the flattening.
        See [1]_, Section 6.5 for details.

        Parameters
        ----------
        it : int, optional
            Number of iterations
            
        Returns
        ------
        FlatMesh object
            Flattened mesh        
        
        Examples
        --------
        >>> from bff import load_mesh
        >>> face = load_mesh('Samples/face.obj')
        >>> face.reorder()
        >>> round_face = mesh.uniformize()
        >>> round_face.plot()
        
        >>> from bff import load_mesh
        >>> face = load_mesh('Samples/face.obj')
        >>> face.reorder()
        >>> round_face = mesh.uniformize()
        >>> round_face.write_to('Samples/round_face.ply')
        File written successfully

        Notes
        -----
        If vertices were not reordered before using the method
        :any:`reorder`, this will be done in the process.
        
        See Also
        --------
        flatten : Boundary First Flattening for specified exterior
            angles or scale factors
        flatten_to_curve : Flattens mesh to shape determined by 
            given boundary curve
        
        References
        ----------
        .. [1] `Sawhney, Rohan, and Keenan Crane. “Boundary First 
           Flattening.” ACM Transactions on Graphics 37.1 (2017): 
           1–14. Crossref. Web. <https://arxiv.org/abs/1704.06873>`_
        """
        
        # Ensure that vertices are reordered
        self.reorder()
        
        # Compute the discrete laplacian and the discrete curvature
        # int_ang attribute will be computed if wasn't previously
        A = self.build_laplacian()
        C = matrix(self.discrete_curvature())
        
        n = self.b_num
        
        # Get starting exterior angles
        k = matrix([2*np.pi/n] * n)
    
        # Compute the sparse cholesky factorization of A and one of 
        # its submatrices
        L = symbolic(A)
        numeric(A, L)
    
        A_II = A[:self.i_num, :self.i_num]
        L_II = symbolic(A_II)
        numeric(A_II, L_II)
        
        # Find the lengths of the boundary edges
        el = self.bound_edge_len()
        
        # Compute mass matrix for later usage
        N_inv = np.diag(el)
        
        for _ in range(it):
            
            # Find complementary boundary data for current value of `k`
            u = _ext_ang_to_scale(L, C, k)
            u = np.array(u).reshape(self.b_num)

            # Compute the target edge lengths using the scale factor so
            # that el_new[i] = np.exp((u[i] + u[i + 1])/2) * el[i]
            el_new = np.exp((u + np.roll(u, -1))/2) * el

            # Compute tangent direction matrix T and its transpose
            phi = np.empty((n,1))
            ang = 0
            for i in range(n):
                ang += k[i]
                phi[i] = ang
                
            T_t = np.concatenate((np.cos(phi), np.sin(phi)), axis=1)
            T = T_t.T
                 
            # Compute new edge lengths as in `best_fit_curve`
            P = N_inv @ T_t
            U = T @ P
            S = P @ np.linalg.inv(U) @ T
            lt = el_new - (S @ el_new)
            
            # Compute dual edges
            ltd = 0.5*(lt + np.roll(lt, 1))
            
            # Compute next iteration of k
            k = np.pi/float(np.sum(ltd))*matrix(ltd) + 0.5*k
        
        # Compute `u` and `el_new` for a final time
        u = _ext_ang_to_scale(L, C, k)
        u = np.array(u).reshape(self.b_num)
        
        el_new = np.exp((u + np.roll(u, -1))/2) * el
        
        # Compute the boundary curve using the iterated k
        gt = best_fit_curve(el, el_new, k)
        
        # Extend the boundary curve holomorphically to the whole 
        # triangle mesh
        x, y = _extend_curve(self, A, L, L_II, gt, holomorphic=False)
        
        return FlatMesh(x, y, self)


    def write_to(self, path):
        """Writes mesh to ``.obj`` or ``.ply`` file
        
        Parameters
        ----------
        path : str
            Path with file name; has to end either with '.obj' or 
            with '.ply'
        
        Examples
        --------
        >>> from bff import load_mesh
        >>> face = load_mesh('Samples/face.obj')
        >>> face.write_to('Samples/face.ply')
        File written successfully
        
        >>> from bff import load_mesh
        >>> from numpy import pi
        >>> mesh = load_mesh('Samples/face.obj')
        >>> mesh.reorder()
        >>> k = [2*pi/mesh.b_num] * mesh.b_num
        >>> flat_face = mesh.flatten(k)
        >>> flat_face.write_to('Samples/flat_face.obj')
        File written successfully
        
        Raises
        ------
        AssertionError
            If no filename extension was found or if filename
            extension is not supported
        """
        
        # Get file extension
        file_ext = path.split('.')[-1]
        
        # Check if there is a file extension
        assert file_ext, 'No filename extension found'
        
        # Check if file extension is supported and write mesh 
        # accordingly
        if file_ext == 'obj':
            return self._write_to_obj(path)
            
        elif file_ext == 'ply':
            return self._write_to_ply(path)
        
        else:
            raise AssertionError('Filename extension not supported')

    
    def _write_to_obj(self, path):
        """Writes mesh to .obj file
        
        Parameters
        ----------
        path : str
            Path with filename
            
        See Also
        --------
        write_to : Calls this method in case of '.obj' filename 
            extension
        """
        with open(path, 'w') as file:
            
            # Write vertices
            for x, y, z in self.v:
                file.write('v %.6f %.6f %.6f\n' % (x, y, z))
                
            # Write faces
            for v1, v2, v3 in self.f:
                file.write('f %.d %.d %.d\n' 
                               % (v1 + 1, v2 + 1, v3 + 1))
                
        print('File written successfully')
        
        
    def _write_to_ply(self, path):
        """Writes mesh to .ply file
        
        Parameters
        ----------
        path : str
            Path with filename
            
        See Also
        --------
        write_to : Calls this method in case of '.ply' filename 
            extension
        """
        # Get the vertex and face numbers
        v_num = len(self.x)
        f_num = len(self.f)
                
        with open(path, 'w') as file:
            
            # Write header
            file.write('ply\n'\
                       'format ascii 1.0\n'\
                       'element vertex %.d\n'\
                       'property float x\n'\
                       'property float y\n'\
                       'property float z\n'\
                       'element face %.d\n'\
                       'property list uint8 int32 vertex_indices\n'\
                       'end_header\n' % (v_num, f_num))
            
            # Write vertices
            for x, y, z in self.v:
                file.write('v %.6f %.6f %.6f\n' % (x, y, z))
                
            # Write faces
            for v1, v2, v3 in self.f:
                file.write('f %.d %.d %.d\n' 
                               % (v1 + 1, v2 + 1, v3 + 1))  

        print('File written successfully')


    def _bound_sort(self):
        """
        Finds the boundary vertices and reorders the vertices of the 
        triangle mesh such that
        
            - the interior vertices come first
            - the boundary vertices are ordered such that consecutive
              vertices are connected by a boundary edge
              
        The attributes `b_num` and `i_num` are written in the process, 
        while the attributes `v` and `f` are being updated.
        
        Safes a dictionary that translates old vertex indices to new
        ones as the attribute `_vdic`.
        
        Notes
        -----
        - The relative order of the non-boundary vertices will be 
          preserved
        - If a list of scalars is provided under the attribute 
          `scalars`, it will be reordered as well
        - You can check the attribute `_sorted` to see if the
          mesh vertices have already been reordered using 
          `_bound_sort`.
            
        Examples
        --------
        >>> from bff import load_mesh
        >>> face = load_mesh('Samples/face.obj')
        >>> face._bound_sort()
        
        >>> from bff import load_mesh
        >>> face = load_mesh('Samples/face.obj')
        >>> face._bound_sort()
        >>> face._vdic[200]
        190
        
        See Also
        --------
        reorder : Calls `_bound_sort`, but checks first if this has 
            been done before
        """
        
        # Initiate set for non-boundary edges
        not_bound_edges = set()
        
        # Initiate set for edges to check if edges occur in more than
        # one face
        edges = set()
                
        # Iterate through edges to find the non-boundary edges,
        # which are the ones that appear in more than one face
        for v1, v2, v3 in self.f:
            
            e1 = frozenset([v1, v2])
            e2 = frozenset([v2, v3])
            e3 = frozenset([v3, v1])
            
            if e1 in edges:
                not_bound_edges.add(e1)
            else:
                edges.add(e1)
        
            if e2 in edges:
                not_bound_edges.add(e2)
            else:
                edges.add(e2)
        
            if e3 in edges:
                not_bound_edges.add(e3)
            else:
                edges.add(e3)
        
        # Find boundary edges
        bound_edges = edges.difference(not_bound_edges)
        
        # Check if there is a boundary
        assert bound_edges, 'No boundary edges found'
        
        # Initiate dictionary to find which boundary vertices are 
        # neighboring each other
        bound_nb = {}
        
        # Fill dictionary:
        for edge in bound_edges:
            p, q = edge
            
            if p in bound_nb:
                bound_nb[p].append(q)
            else:
                bound_nb[p] = [q]
                
            if q in bound_nb:
                bound_nb[q].append(p)
            else:
                bound_nb[q] = [p]
        
        # Find number of boundary and interior vertices
        self.b_num = len(bound_nb)
        self.i_num = self.v_num - self.b_num 
        
        # Initiate list for ordered boundary vertices
        # First, check if bound_edges is empty
        bound_ind = [p, q]
        
        # Walk along the boundary using the dictionary and fill the 
        # list of the boundary vertices
        for i in range(self.b_num - 2):
            
            # Get the last two vertices
            v = bound_ind[-1]
            w = bound_ind[-2]
            
            # Check which one of the boundary neighbors of v is not w 
            # and append it to list
            if w != bound_nb[v][0]:
                bound_ind.append(bound_nb[v][0])
                
            else:
                bound_ind.append(bound_nb[v][1]) 
        
        # Check if bound_ind contains all boundary vertices
        assert len(bound_ind) == self.b_num, 'Triangle mesh is not '\
                                              + 'disk-like'
        
        # Find the indices of the non-boundary vertices
        S = set(bound_ind)
        not_bound_ind = [i for i in range(self.v_num) if i not in S]
    
        # Initiate list for the new v attribute        
        v_new = []
        
        # Initiate dictionary that translates old vertex indices to 
        # new ones
        dic = {}
        
        # Fill dictionary and list
        for new_ind, old_ind  in enumerate(chain(not_bound_ind, bound_ind)):
            
            v_new.append(self.v[old_ind])
            dic[old_ind] = new_ind
        
        # Update vertex list
        self.v = v_new
        
        # Update face array with new vertex indices
        self.f = [(dic[v1], dic[v2], dic[v3]) for v1, v2, v3 in self.f]
        
        # Update scalars data if necessary
        if not isinstance(self.scalars, str):
            scalars = self.scalars
            self.scalars = [scalars[dic[i]] for i in range(self.v_num)]
        
        self._sorted = True
        self._vdic = dic

    
    def _find_int_ang(self):
        """
        Computes the interior angles of the triangle mesh and writes 
        them to the `int_ang` attribute.
        """
        
        # Covert indices from face data into vertices and 
        # build a 3d array with it
        V = [[self.v[f1], self.v[f2], self.v[f3]] for f1, f2, f3 in self.f]
        V = np.array(V)
        
        # Cyclic shift the array by one and compute edge lengths
        V_shift = np.roll(V, 1, axis=1)
            
        E1 = np.linalg.norm(V - V_shift, axis=2)

        # Get cyclic shifts of the edge length array
        E2 = np.roll(E1, 2, axis=1)
        E3 = np.roll(E1, 1, axis=1)
        
        # Compute interior angles using the formula 
        # arccos((k**2 + l**2 - m**2)/(2*k*l)) for k, l, m the side 
        # lengths of each face triangle
        self.int_ang = np.arccos((E1**2 + E2**2 - E3**2)/(2*E1*E2)).tolist()
                      
        return



class FlatMesh(TriMesh):
    """Class to process triangle meshes after being flattened
    
    Subclass of `TriMesh` to process triangle meshes after being 
    flattened by the method `flatten`. 
    Overwrites the method `plot` to plot in 2d.
    
    Parameters
    ----------
    x : array_like
        x coordinates of the vertices of the flattened mesh
    y : array_like
        y coordinates of the vertices of the flattened mesh
    mesh : TriMesh object
        Triangle mesh which was flattened
    
    Attributes
    ----------
    v_old : list of list of float
        Coordinates of the vertices before flattened. Used primarily 
        for plotting purposes.
        
        
    """
    
    def __init__(self, x, y, mesh):
        
        # Write data to attributes
        v = [[xi, yi, 0] for xi, yi in zip(x,y)]
        
        super().__init__(v, mesh.f)
        
        if mesh._sorted:
            self._sorted = True
            self._vdic = mesh._vdic
            self.i_num = mesh.i_num
            self.b_num = mesh.b_num
        
        # Check if mesh was already flattened and write `v_old`
        # accordingly
        if isinstance(mesh, FlatMesh):
            self.v_old = mesh.v_old
        else:
            self.v_old = mesh.v
        
        # Extract scalar data from old mesh
        self.scalars = mesh.scalars

        # Extract colormap from old mesh
        self.cmap = mesh.cmap

    
    def plot(self):
        """Plots mesh in 2d
        
        Plots a flattened triangle mesh using `matplotlib.pyplot.
        tricontourf`.
        
        Examples
        --------
        >>> from bff import load_mesh
        >>> from numpy import pi
        >>> mesh = load_mesh('Samples/face.obj')
        >>> mesh.reorder()
        >>> k = [2*pi/mesh.b_num] * mesh.b_num
        >>> flat = mesh.flatten(k)
        >>> flat.plot()
        """
        
        # Get vertex data
        x = [vertex[0] for vertex in self.v]
        y = [vertex[1] for vertex in self.v]
        
        # Determine `scalars` value for plotting
        if type(self.scalars) != str:
            scalars = self.scalars
        
        elif self.scalars == 'z':
            scalars = [v[2] for v in self.v_old]
            
        elif self.scalars == 'x':
            scalars = [v[0] for v in self.v_old]
            
        elif self.scalars == 'y':
            scalars = [v[1] for v in self.v_old]
            
        plt.tricontourf(x, y, self.f, scalars, 100,
                        cmap=self.cmap)
        plt.axis('equal')
        plt.show()


#===============================Functions======================================


def best_fit_curve(w, el, k):
    r"""Closes curve by adjusting edge lengths
    
    For a discrete curve specified by edge lengths :math:`el` and 
    exterior angles `k`, find new edge lengths :math:`\tilde{el}` 
    such that the curve closes and
    
    .. math::
        \sum_{ij} w_i^{-1} (\tilde{el}_{ij} - el_{ij})^2
        
    is minimized for weights :math:`w`.
    
    Parameters
    ----------
    w : list or (n,) ndarray
        Weights in the formula above
    el : list or (n,) ndarray
        Edge lengths of the curve
    k : array_like
        Exterior angles along the curve; have to sum up 
        to :math:`2\pi`
        
    Returns
    -------
    (n, 2) ndarray
        Closed curve determined by new edge lengths
        
    Examples
    --------
    >>> from bff import best_fit_curve
    >>> from numpy import pi
    >>> k = [pi/2, pi/2, pi/2, pi/2]
    >>> el = [1, 1, 1, 2]
    >>> best_fit_curve([1,1,1,1], el, k)
    array([[ 0.00000000e+00,  0.00000000e+00],
           [ 6.12323400e-17,  1.00000000e+00],
           [-1.50000000e+00,  1.00000000e+00],
           [-1.50000000e+00,  5.55111512e-16]])
    """
    # Find vertex number of curve
    n = len(w)
    
    # Building tangent direction matrix T and the inverse of diagonal 
    # mass matrix N
    phi = np.empty((n,1))
    ang = 0
    for i in range(n):
        ang += k[i]
        phi[i] = ang
        
    # Compute transpose of T first
    T_t = np.concatenate((np.cos(phi), np.sin(phi)), axis=1)
    T = T_t.T
    
    N_inv = np.diag(w)

    # Compute the exact new boundary edge lengths
    ls = np.array(el)
    
    P = N_inv @ T_t
    U = T @ P
    S = P @ np.linalg.inv(U) @ T
    lt = ls - (S @ ls)

    # Compute new boundary curve
    curve = np.zeros((n, 2))
    
    for i in range(n - 1):
        curve[i+1] = curve[i] + lt[i]*T_t[i]
    
    return curve


def load_mesh(path):
    """Loads triangle mesh from .obj or .ply file
    
    Parameters
    ----------
    path : str
        Path including filename to the .obj or .ply file
        
    Returns
    -------
    TriMesh object
        Loaded triangle mesh
        
    Examples
    --------
    >>> from bff import load_mesh
    >>> mesh = load_mesh('Samples/face.obj')
    
    >>> from bff import load_mesh
    >>> bunny = load_mesh('Samples/bunny.obj')
    >>> bunny_head = bunny.cut_out(1, 50)
    >>> bunny_head.plot()
    
    Raises
    ------
    AssertionError
        If no filename extension was found or if the filename 
        extension is not supported
    """
    
    # Get file extension
    file_ext = path.split('.')[-1]
    
    # Check for file extension
    assert file_ext, 'no filename extension found'
    
    # Import data according to file extension
    if file_ext == 'obj':
        return _mesh_from_obj(path)
        
    elif file_ext == 'ply':
        return _mesh_from_ply(path)
        
    else:
        raise AssertionError('Filename extension not supported')

    
def _mesh_from_obj(path):
    """
    Loads .obj file and extracts vertex and face informations. These 
    are then returned as a TriMesh object.
    
    Parameters
    ----------
    path : str
        Path to an .obj file

    Returns
    -------
    TriMesh object
        Loaded triangle mesh
        
    See Also
    --------
    load_mesh : Calls this function in case of '.obj' filename 
        extension
    """
    
    # Load file
    with open(path) as file:
        
        # Initiate empty lists for vertices and faces
        v = []
        f = []
        
        #Iterate through file
        for line in reader(file, delimiter = ' ', skipinitialspace = True):
            
            # Check for empty lines
            if not line:
                continue
            
            # Gather vertices
            if line[0] == 'v':
                
                line.pop(0)
                v.append(tuple(float(string) for string in line))
                
            # Gather faces.
            elif line[0] == 'f':
                line.pop(0)

                # Remove empty string which is possibly there due to a
                # whitespace at the end of the line
                if line[-1] == '':
                    line.pop(-1)
                    
                # If needed, extract the vertex index from expressions 
                # of the form '8/1/5'
                if '/' not in line[0]:
                    f.append(tuple(int(string) - 1 for string in line))
                    
                else:
                    f.append(tuple(int(string.split('/')[0])
                                       - 1 for string in line))
    
    return TriMesh(v, f)


def _mesh_from_ply(path):
    """
    Loads .ply file and extracts vertex and face informations. These
    are then returned as a TriMesh object.
    
    Parameters
    ----------
    path : str
        Path to a .ply file
    
    Returns
    -------
    TriMesh object
        Loaded triangle mesh
    
    See Also
    --------
    load_mesh : Calls this function in case of '.ply' filename 
        extension
    """
    
    # Load file
    with open(path) as file:
        
        # Initiate empty lists for vertices and faces
        v = []
        f = []
        
        reader_ = reader(file, delimiter = ' ', skipinitialspace = False)
        
        # Read through header to find face/vertex numbers
        for line in takewhile(lambda x: x!= ['end_header'], reader_):
            
            if line[0] == 'element':
                
                if line[1] == 'vertex':
                    v_num = int(line[2])

        # Find vertices and faces
        for i, line in enumerate(reader_):
            
            if i < v_num:
                v.append(tuple(float(string) for string in line[:3]))
                
            if i >= v_num:
                f.append(tuple(int(string) for string in line[1:4]))
                
    return TriMesh(v, f)


def _ext_ang_to_scale(L, C, k):
    """Computes compatible scale factor for given exterior angles
    
    Parameters
    ----------
    L : PyCapsule object
        Cholesky factorization of the Laplacian of the triangle mesh 
        via `cvxopt.cholmod.numeric`. See the `CVXOPT User's Guide
        <http://cvxopt.org/userguide/spsolvers.html#positive-definite-
        linear-equations>`_ for details.
    C : cvxopt.base.matrix
        Discrete curvature of the triangle mesh using the discrete 
        Gaussian curvature at interior and the discrete geodesic 
        curvature at boundary vertices
    k : cvxopt.base.matrix
        Exterior angles at the boundary vertices
 
    Returns
    -------
    cvxopt.base.matrix
        Compatible scale factors
    """
    
    # Get number of interior vertices
    i_num = len(C) - len(k)

    # Get the the r.h.s of the linear equation in question
    b = matrix([-C[: i_num], -C[i_num :] + k])

    solve(L, b)
    
    return b[i_num :] 


def _scale_to_ext_ang(A, L_II, C, u):
    """Computes compatible exterior angles for given scale factors
    
    Parameters
    ----------
    A : cvxopt.base.spmatrix
        Discrete Laplacian of the triangle mesh as computed with 
        `build_laplacian`
    L_II : PyCapsule object
        Cholesky factorization of a submatrix of A computed with 
        `cvxopt.cholmod.numeric`. See the `documentation <https:
        //conformal-flattening.readthedocs.io/en/latest/math.html
        #the-cherrier-formula>`_ and the `CVXOPT User's Guide 
        <http://cvxopt.org/userguide/spsolvers.
        html#positive-definite-linear-equations>`_ for details.
    C : cvxopt.base.matrix
        Discrete curvature of the triangle mesh using the discrete 
        Gaussian curvature at interior and the discrete geodesic 
        curvature at boundary vertices
    u : cvxopt.base.matrix
        Scale factors at boundary vertices 
        
    Returns
    -------
    cvxopt.base.matrix
        Compatible exterior angles
    """
    
    i_num = len(C) - len(u)
    
    A_IB = A[:i_num, i_num:]
    A_BB = A[i_num:, i_num:]
    
    a = - C[:i_num] - A_IB*u
    
    solve(L_II, a)
    
    return C[i_num:] + A_IB.T*a + A_BB*u


def _extend_curve(mesh, A, L, L_II, curve, holomorphic=True):
    """Extends boundary curve holomorphically to the whole mesh
    
    Extends a complex valued curve defined on the boundary of a 
    triangle mesh holomorphically to the whole mesh.
    
    Parameters
    ----------
    mesh : TriMesh object
        Triangle mesh in question
    A : cvxopt.base.spmatrix
        Discrete Laplacian of the triangle mesh as computed with 
        :any:`build_laplacian`
    L : PyCapsule object
        Cholesky factorization of `A` computed with 
        `cvxopt.cholmod.numeric`. See the `documentation <https:
        //conformal-flattening.readthedocs.io/en/latest/math.html
        #the-cherrier-formula>`_ and the `CVXOPT 
        User's Guide <http://cvxopt.org/userguide/spsolvers.
        html#positive-definite-linear-equations>`_ for details.
    L_II : PyCapsule object
        Cholesky factorization of a submatrix of A computed with 
        `cvxopt.cholmod.numeric`. See the `documentation <https:
        //conformal-flattening.readthedocs.io/en/latest/math.
        html#the-cherrier-formula>`_ and the `CVXOPT 
        User's Guide <http://cvxopt.org/userguide/spsolvers.
        html#positive-definite-linear-equations>`_ for details.
    curve : (mesh.b_num, 2) ndarray
        Target boundary curve as computed with :any:`best_fit_curve`
    holomorphic : bool, optional
        If `True`, the curve will be extended using harmonic 
        conjugation. If `False`, a harmonic extension will be 
        used instead. See [1]_, Section 4.4 for details. 
        Defaults to `True`.
        
    Returns
    -------
    array_like
        Real part of the extension
    array_like
        Imaginary part of the extension
        
    References
    ----------
    .. [1] `Sawhney, Rohan, and Keenan Crane. “Boundary First 
       Flattening.” ACM Transactions on Graphics 37.1 (2017): 
       1–14. Crossref. Web. <https://arxiv.org/abs/1704.06873>`_
    """
    # Get relevant submatrix of the Laplacian
    A_IB = A[:mesh.i_num, mesh.i_num:]
    
    # Get the real part of `curve`
    re_c = matrix(curve[:,0])
    
    # Solve the Dirichlet problem for the harmonic extension of the
    # boundary curve
    a = -A_IB * re_c
    solve(L_II, a)
    
    a = matrix([a, re_c])
    
    
    # If holomorphic=True, compute b via the harmonic conjugation
    if holomorphic:
        # Compute the Hilbert transform
        h = [0] * mesh.i_num
        h += [0.5*(re_c[i - 1] - re_c[i + 1]) for i in range(mesh.b_num - 1)]
        h.append(0.5*(re_c[mesh.b_num - 2] - re_c[0]))
        b = matrix(h)
        
        # Solve the von Neumann problem for the  harmonic conjugation 
        solve(L, b)    
        
    # Otherwise, extend b harmonically
    else:
        # Get the imaginary part of curve
        im_c = matrix(curve[:,1])
        
        # Solve the corresponding Dirichlet problem
        b = -A_IB * im_c
        solve(L_II, b)
        
        b = matrix([b, im_c])

    return a, b

def example_curve(x):
    r"""Example curve for :any:`flatten_to_curve`
    
    Arc length parametrization for the oriented curve 
    :math:`(\cos(t)^3, \sin(t)^3)`. The curve has length 6.
    
    Parameters
    ----------
    x : float
    
    Returns
    -------
    complex or None
        Returns point in curve if `x` is in the closed 
        interval from 0 to 6 and `None` otherwise
    """

    if 0 <= x < 3/2:
        a = np.sqrt(1 - 2/3*x)**3
        b = np.sqrt(2/3*x)**3
        
    elif 3/2 <= x <3:
        a = - np.sqrt(2/3*x - 1)**3
        b = np.sqrt(2 - 2/3*x)**3
        
    elif 3 <= x < 9/2:
        a = - np.sqrt(3 - 2/3*x)**3
        b = - np.sqrt(2/3*x - 2)**3
        
    elif 9/2 <= x <= 6:
        a = np.sqrt(2/3*x - 3)**3
        b = - np.sqrt(4 - 2/3*x)**3
        
    else:
        return
    
    return a + b*1j