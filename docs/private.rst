.. currentmodule:: bff

Private funtions of the bff module
----------------------------------

.. autofunction:: _ext_ang_to_scale

.. autofunction:: _scale_to_ext_ang

.. autofunction:: _extend_curve

.. autofunction:: _mesh_from_obj

.. autofunction:: _mesh_from_ply


Private methods of the TriMesh class
------------------------------------

.. automethod:: TriMesh._bound_sort

.. automethod:: TriMesh._find_int_ang

.. automethod:: TriMesh._write_to_obj

.. automethod:: TriMesh._write_to_ply
