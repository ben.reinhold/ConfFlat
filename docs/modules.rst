Documentation
=============

.. automodule:: bff
   :members: load_mesh, best_fit_curve

The TriMesh class
-----------------

The `TriMesh` class is the core of the ``bff`` module. You can load `TriMesh` objects from ``.obj`` or ``.ply`` files using the function :any:`load_mesh` above. 

To flatten  `TriMesh` objects with the boundary first flattening algorithm, you can use :any:`reorder` in combination with :any:`flatten` for custom boundary data or :any:`uniformize` to obtain a 2d disk shape. 

See the page :doc:`First Steps <firststeps>` or the ``examples.ipynb`` jupyter notebook in the `GitLab repository <https://gitlab.gwdg.de/ben.reinhold/ConfFlat>`_ for usage examples.

.. autoclass:: TriMesh
   :members:


The FlatMesh subclass
---------------------
   
The class `FlatMesh` is a subclass of `TriMesh` and is used to process the triangle mesh after being flattened using :any:`flatten` or :any:`uniformize`. 

The differences to the `TriMesh` class are that the method `plot` will plot the mesh in 2d instead of 3d and that an additional attribute `v_old` is added that contains the pre-flatten vertex data. The latter is used to color the flattened mesh in the same way as the original one when using the method `plot`.

To write the flattened mesh to a ``.obj`` or ``.ply`` file, use the method :any:`write_to`.
   
.. autoclass:: FlatMesh
   :members:

--------------

**Private functions and methods**

You can find a documentation of the private functions and methods :doc:`here <private>`.
   
