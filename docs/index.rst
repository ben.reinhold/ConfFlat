.. ConfFlat documentation master file, created by
   sphinx-quickstart on Sun Sep 29 12:34:18 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ``bff``'s documentation!
====================================

The ``bff`` module implements the Boundary First Flattening algorithm presented in the same titled `paper <https://arxiv.org/abs/1704.06873>`_ by Rohan Sawhney and Keenan Crane in Python.

It was created as part of the course "Advanced practical course in scientific computing" at the University of Göttingen under the advision of Dr. Jochen Schulz and Prof. Max Wardetzky. You can find its GitLab repostitory `here <https://gitlab.gwdg.de/ben.reinhold/ConfFlat>`_.



.. toctree::
   :maxdepth: 3
   :caption: Contents:
   
   firststeps
   math
   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
