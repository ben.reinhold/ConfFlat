First Steps
===========

For a more interactive overview of the main module functions in form of a jupyter notebook, see ``examples.ipynb`` in the `GitLab repository <https://gitlab.gwdg.de/ben.reinhold/ConfFlat>`_.

Loading a triangle mesh from file
---------------------------------
You can load triangle meshes from ``.ply`` or ``.obj`` files using the function :any:`bff.load_mesh`. This will return a :any:`TriMesh` object that can be plotted in 3d using the method :any:`plot <bff.FlatMesh.plot>`::

    >>> from bff import load_mesh
    >>> mesh = load_mesh('Samples/face.obj')
    >>> mesh.plot()
    
.. image:: _example_pictures/mesh.png

Reordering the vertices
-----------------------
To use the Boundary First Flattening algorithm, you have to specify exterior angles or scale factors for each boundary vertex. For this, you first have to reorder the vertices using the method :any:`reorder`. This will find the boundary of the mesh and do the following:

- The attributes `b_num` and `i_num` will be added to the object `mesh`, containing the number of boundary and interior vertices, respectively
- The vertices of the triangle mesh (stored under `mesh.v`) will be reordered such that:
    - The interior vertices come before the boundary ones
    - The boundary vertices are ordered such that consecutive boundary vertices are connected by a boundary edge
    
:: 

    >>> mesh.reorder()
    >>> mesh.b_num
    168
    
Specifying boundary data and flattening the mesh
------------------------------------------------
Note that if you want to flatten the mesh by specifying exterior angles, these have to add up to :math:`2\pi`.
::

    >>> from numpy import pi
    >>> k = [2*pi/mesh.b_num for _ in range(mesh.b_num)]   # Example exterior angles
    >>> u = [0 for _ in range(mesh.b_num)]   # Example scale factors
    
You can then flatten the mesh with respect to your boundary data using the method :any:`flatten`. This will return a :any:`FlatMesh` object that can be plotted in 2d or written to a ``.ply`` or ``.obj`` file using the methods :any:`plot <bff.FlatMesh.plot>` and :any:`write_to`, respectively::

    >>> flat_face = mesh.flatten(k)   # Specifying exterior angles
    >>> flat_face.plot()
    
.. image:: _example_pictures/flat_face.png

::

    >>> flat_face.write_to('Samples/flat_face.obj')
    File written succesfully

By default, :any:`flatten` expects to be handed exterior angles. To use scale factors instead, use the keyword argument `scale_factors=True`::
    
    >>> another_flat_face = mesh.flatten(u, scale_factors=True)
    >>> another_flat_face.plot()
    
.. image:: _example_pictures/another_flat_face.png

Sharp Corners
-------------
The exterior angles `k` are by default not necessarrily realised, but only approximated. If you want to realise the exterior precisely, for example if you have specified sharp corners, you can call the method :any:`flatten` with the keyword argument `holomorphic=False`::

    >>> k = ([pi/2] + [0]*41)*4   # Recall that mesh.b_num = 168 = 42*4
    >>> square_face = mesh.flatten(k, holomorphic=False)
    >>> square_face.plot()
    
.. image:: _example_pictures/square_face.png

Uniformization
--------------
To flatten the triangle mesh to a disk, you can use the method :any:`uniformize`. You don't have to reorder the vertices before using this method, since you don't assign boundary data to individual vertices. But if not done before, this will be done in the process.

::

    >>> round_face = mesh.uniformize()
    >>> round_face.plot()
    
.. image:: _example_pictures/round_face.png

Cutting the mesh
----------------
The :any:`TriMesh` module also contains methods that either remove or cut out from the mesh all vertices in a given range around a specified vertex. These are the methods :any:`cut` and :any:`cut_out`, respectively.

Note that the Boundary First Algorithm only works for disk-like meshes. For a rough indication, you can compute the Euler characteristic of the mesh using the method :any:`euler_char`::

    >>> bunny = load_mesh('Samples/bunny.obj')
    >>> bunny_head = bunny.cut_out(1, 50)
    >>> bunny_head.euler_char()
    1
    >>> bunny_head.plot()
    
.. image:: _example_pictures/bunny_head.png

::

    >>> bunny_head.reorder()
    >>> s = [0] * bunny_head.b_num   # Scale factors
    >>> flat_head = bunny_head.flatten(s, scale_factors=True)
    >>> flat_head.plot()
    
.. image:: _example_pictures/flat_head.png

::

    >>> bunny_with_hole = bunny.cut(0, 30)
    >>> bunny_with_hole.euler_char()
    1
    >>> bunny_with_hole.plot()
    
.. image:: _example_pictures/bunny_with_hole.png

::

    >>> flat_bunny = bunny_with_hole.uniformize()
    >>> flat_bunny.plot()
    
.. image:: _example_pictures/flat_bunny.png


Specifying a boundary curve
---------------------------
You can also flatten the mesh to a prescribed form by specifying a closed boundary curve in form of a Python function. Note, however, that the curve has to be oriented and in arc-length parametrization. 

As an example, you can find an arc-length parametrized version of the curve :math:`(\cos(t)^3, \sin(t)^3)` under ``bff.example_curve``. To flatten the mesh, call the method :any:`flatten_to_curve` and hand over the curve and its length (in our case this is `6`)::

    >>> from bff import example_curve
    >>> spiky_face = mesh.flatten_to_curve(example_curve, 6, 20)
    >>> spiky_face.plot()

.. image:: _example_pictures/spiky_face.png

You can specify the number of iterations used to fit the mesh into the curve by calling the method with a third argument. The number of iterations defaults to `20`.

::

    >>> spiky_head = bunny_head.flatten_to_curve(example_curve, 6, 40)
    >>> spiky_head.plot()
    
.. image:: _example_pictures/spiky_head.png
