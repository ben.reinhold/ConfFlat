Boundary First Flattening
=========================

In this section, we provide a short overview of the mathematics involved in the Boundary First Flattening algorithm, closely following :cite:`SC17`.

The smooth setting
------------------

Let :math:`M` be a two-dimensional oriented disk-like smooth manifold equipped with a Riemannian metric :math:`g`. Let :math:`g'` be another Riemannian metric on :math:`M`. We say that :math:`g` and :math:`g'` are *conformally equivalent* if :math:`g' = e^{u} g` for :math:`u\colon M\to \mathbb R` a smooth function. This is equivalent to :math:`g` and :math:`g'` inducing the same angles between elements of :math:`T_pM`, :math:`p\in M`.

For :math:`p\in M` and :math:`X\in T_p M`, we define :math:`\mathcal J X\in T_p M` by rotating :math:`X` by :math:`90^\circ` in the counter-clockwise direction. This is easily seen to define an *almost complex structure* on :math:`M`, which depends on :math:`g` only up to conformal equivalence. Let :math:`(\tilde M, \tilde g)` be another two-dimensional oriented smooth Riemannian manifold with induced almost complex structure :math:`\tilde{\mathcal J}` and :math:`f\colon M\to \tilde M` be a smooth map. We call :math:`f` *holomorphic* if :math:`df\colon TM\to T\tilde M` commutes with the almost complex structures, that is

.. math::
    df \circ \mathcal{J} = \tilde{\mathcal{J}} \circ df.
    
Note that by dimensional reasons, :math:`\mathcal J` and :math:`\tilde{\mathcal J}` are always `integrable <https://en.wikipedia.org/wiki/Almost_complex_manifold#Integrable_almost_complex_structures>`_. Holomorphicity of :math:`f` is then equivalent to the Cauchy-Riemann equations holding in local complex coordinates.

If :math:`f` is a smooth immersion, that is, :math:`df` is tangent space wise injective, on top of being holomorphic, we call :math:`f` *conformal*. This is equivalent to :math:`f^* {\tilde g}` being conformally equivalent to :math:`g`. The smooth map :math:`u\colon M\to \mathbb R` such that :math:`f^* \tilde g = e^u g` is called the *log conformal scale factor*.
    

**Conformal Flattening**

In the following, we only consider the case where :math:`\tilde M = \mathbb C`, where :math:`\mathbb C` carries its canonical Riemannian metric. Identifying the tangent spaces of :math:`\mathbb C` with :math:`\mathbb C` itself, :math:`\mathcal J` can be identified with the multiplication by :math:`i`. The holomorphicity condition for a smooth map :math:`f\colon M\to \mathbb C` then becomes

.. math::
    df(\mathcal{J} X) = i df(X)
    
for all :math:`X\in TM`. Using that :math:`\ast df(X) = df(\mathcal{J}X)` for all :math:`X\in TM`, where :math:`\ast` is the *Hodge star operator*, one can write this more concisely as :math:`\ast df = i df`.

If :math:`f= a+ ib\colon M\to \mathbb C` is conformal, we call :math:`f` a *conformal flattening of* :math:`M`. In this case, one can show that

.. math:: 
    \Delta a = \Delta b =0,
    
where :math:`\Delta` is the Laplace-Beltrami operator on :math:`M`. In particular, :math:`f` is already uniquely determined by :math:`\gamma = f|_{\partial M}`. But in general, curves :math:`\gamma\colon \partial M\to \mathbb C` can not always be extended to a conformal map on the interior of :math:`M`.

However, a generalization of the *Riemann mapping theorem* states that for each disk-like region :math:`D\subset \mathbb C`, one can find a conformal map :math:`f\colon M\to D` mapping :math:`\partial M` to :math:`\partial D`. So while in general not every boundary curve :math:`\gamma\colon \partial M\to \mathbb C`  enclosing a disk-like domain can be realised by a conformal flattening of :math:`M`, its shape :math:`\gamma(\partial M)=\partial D` always can be.

The discrete setting
--------------------
In the discrete setting, we approximate :math:`M` by a disk-like triangle mesh :math:`M=(V, E, F)` with specified edge lengths :math:`E\to \mathbb{R}_{+}`. We index the set of vertices :math:`V` by integers :math:`i=1,\dotsc,|V|` and edges and faces by tuples of two and three vertex indices, respectively.

**Discrete Curvature**
For :math:`ijk\in F`, we denote by :math:`\beta_i^{jk}` the interior angle of the face at the vertex with index :math:`i`. The *discrete Gaussian curvature* :math:`\Omega` at a vertex :math:`i\in V` is given by the angle defect

.. math:: \Omega_i = 2\pi - \sum_{ijk\in F} \beta_i^{jk}

at interior vetices and is zero at boundary vertices. One can consider :math:`\Omega_i` the integral of the *Gaussian curvate* around a small neighborhood of :math:`i`. 

At boundary vertices, the quantity of interest is the *discrete geodesic curvature*

.. math:: k_i = \pi - \sum_{ijk\in F} \beta_i^{jk},

which can be thought of as the *geodesic curvature* of the boundary curve intergrated along a small neighborhood of :math:`i` in :math:`\partial M`. For planar triangle meshes, these are the *exterior angles* of the boundary curve and measure the change of tangent directions going from one boundary edge to the next

Discrete Poisson problems
-------------------------
In the continuous setting, the Poisson problem on :math:`M` is about finding a solution to the *Poisson equation*

.. math::
    \Delta a = \phi,
    
where :math:`\Delta` is the Laplace-Beltrami operator on :math:`M`, 
subject to given boundary conditions. Common boundary conditions on :math:`\partial M` are

.. math::
    a = g \quad \text{and} \quad \frac{\partial a}{\partial n} = f,
    
where :math:`f` and :math:`g` are sufficiently smooth functions on :math:`\partial M` and :math:`n` is the outward normal along the boundary. These are called the *Dirichlet* and *Neumann boundary conditions*, respectively.

Integrating the Poisson equation over "dual cells" yields the matrix equation

.. math::
    \begin{pmatrix} A_{II} & A_{IB} \\ A_{IB}^T & A_{BB} \end{pmatrix} \begin{pmatrix} a_I \\ a_B \end{pmatrix} = \begin{pmatrix} \phi_I \\ \phi_B - h\end{pmatrix},
    
where :math:`\phi` and :math:`h` are the source term in the Poisson equation and the Neumann boundary data :math:`f` intergrated over "dual cells" and "dual boundary edges", respectively. The matrix :math:`A` can be computed with the *Cotan Formula*. See :cite:`CdGDSchroder13` Chapter 6 and :cite:`SC17` Sections 3.3. and 4.1. for details. 

In the equation above, the vertex indices are ordered such that the indices of the interior vertices come before ones of the boundary vertices and the matrix :math:`A` is decomposed into block matrices accordingly.

While the equation above yields immediately a solution of the Poisson equation for Neumann boundary conditions :math:`h`, a solution for the Poisson-Dirichlet problem with boundary data :math:`g = a_B` is obtained by solving the first row for :math:`a_I`, which can be rewritten as 

.. math:: 
    A_{II} a_I = \phi_I - A_{IB} g.

.. rubric:: Poincare-Steklov operators

For a given Poisson equation with source :math:`\phi`, the maps mapping Dirichlet boundary data to the corresponding Neumann boundary data yielding the same solution and vice versa are called *Poincare-Steklov operators*. Solving the discrete Poisson equation for :math:`h` and :math:`g=a_B`, respectively, one gets for :math:`h`

.. math:: 
    h = \phi_B - A_{IB}^TA_{II}^{-1} (\phi_I - A_{IB} g) - A_{BB}g,

while :math:`a_B` is obtained by just solving the equation for :math:`a= ( a_I, a_B )^T`.

The Cherrier formula
--------------------
For :math:`f\colon M \to \tilde M` a conformal map between Riemannian surfaces, the *Cherrier formula* relates the conformal scale factor :math:`u` of :math:`f` to the Gaussian and geodesic curvatures on :math:`M, \tilde M` and :math:`\partial M, \partial \tilde M`, respectively. After discretizing and intergrating over dual cells, it reads

.. math::
    \begin{aligned}Au &= \Omega - \tilde \Omega - \begin{pmatrix}0 \\ h\end{pmatrix}, \\ h &= k - \tilde k,\end{aligned}
    
which is a discrete Poisson-Neumann problem. One can then compute :math:`u|_{\partial M}` from :math:`\tilde k` and vice versa using the corresponding Poincare-Steklov operators.


The algorithm
-------------
The Boundary First Flattening algorithm now consists of the following steps. With the target boundary curve either specified by target exterior angles :math:`\tilde k` or scale factors :math:`u` along the boundary:

- Compute complementary boundary data by using the corresponding Poincare-Steklov operators to compute compatible scale factors :math:`u` if exterior angles :math:`\tilde k` where specified, or compatible exterior angles :math:`\tilde k` if scale factors :math:`u` were given.

- Approximate target edge lengths by :math:`l^*_{ij} = e^{(u_i+u_j)/2} l_{ij}`. Then minimally adjust these to edge lenghts :math:`\tilde l_{ij}` such that the discrete curve :math:`\tilde \gamma` specified by :math:`\tilde l` and :math:`\tilde k` closes.

- Extend the boundary curve :math:`\tilde\gamma` holomorphically to the interior of the mesh.


Closing the curve
-----------------
Given exterior angles :math:`\tilde k \colon B \to \mathbb R` and target edge lengths :math:`\{l^*_{ij}\}_{ij \in \partial M}`, we can reconstruct the boundary curve as follows:

For convenience, we consider the boundary vertices to be indexed by :math:`i=1,\dotsc, |B|` such that vertices with consecutive indices are connected by a positively oriented boundary edge. We also combine boundary edge lengths into :math:`(|B|\times 1)`-vectors of the form :math:`l=(l_{12},\dotsc, l_{|B|1})^T`. 
Given tangents at the boundary vertices defined by :math:`T_i = (\cos (\phi_i), \sin(\phi_i))^T` for :math:`\phi_i = \sum_{j=1}^i \tilde k_j`, we can reconstruct the corresponding discrete curve :math:`\tilde\gamma` via :math:`\tilde\gamma_i = \sum_{j=1}^{i-1}  l^*_{j} T_{j}`. 

However, the curve :math:`\tilde \gamma` might not be closed, even if the exterior angles sum up to :math:`2\pi`. To fix this, we impose new boundary edge lengths :math:`\tilde l\in \mathbb R^{|B|\times 1}` that minimize

.. math ::
    \frac{1}{2}\sum_{i=1}^{|B|} l_{i}^{-1}|\tilde l_{i} - l^*_{i}|^2
    
subject to the boundary condition 

.. math ::
    \sum_{i=1}^{|B|} \tilde l_{i} T_i = 0.
    
By considering :math:`(2\times |B|)` and :math:`(|B|\times |B|)` matrices :math:`T` and :math:`N`, respectively, defined by 

.. math :: 
    T = \begin{pmatrix} T_1 & \dots & T_{|B|}\end{pmatrix}, \qquad N= \begin{pmatrix}  l_{12}^{-1} & & \\ & \ddots & \\ & & l_{|B|1}^{-1} \end{pmatrix},
    
we can rewrite these equations as

.. math ::
    \frac{1}{2} (\tilde l - l^*)^T N (\tilde l - l^*) \quad \text {such that} \quad T\tilde l = 0.
    

Using lagrange multipliers :math:`\lambda = (\lambda_1, \lambda_2)^T`, this minimization problem can be reduced to solving

.. math ::
    \begin{pmatrix} N & T \\ T^T & 0\end{pmatrix} \begin{pmatrix} \tilde l \\ \lambda\end{pmatrix} = \begin{pmatrix}N l^* \\ 0\end{pmatrix}.

One can then check that :math:`\tilde l = l^* - N^{-1} T^T (T N^{-1} T^T)^{-1} T l^*` solves this for :math:`\lambda = (T N^{-1} T^T)T l^*`.

Extending the curve
-------------------
We then have to extend the discrete curve :math:`\tilde\gamma` determined by :math:`\tilde k` and :math:`\tilde l` to the interior vertices in a holomorphic fashion to obtain the desired conformal flattening :math:`f = a + ib \colon V \to \mathbb C`. We proceed seperately for the real and for the imaginary part. We first extend the real part :math:`a` hamornically by solving 

.. math :: 
    \Delta a =0 \quad \text{with} \quad a|_{\partial M} = \text{Re}(\tilde \gamma)
    
in the discrete setting (see `above <#discrete-poisson-problems>`_).

To interpolate the imaginary part of :math:`f`, we seek to minimize the conformaly energy

.. math:: \int_M |\ast df - i df|^2 dA,

which measures the failure of the Cauchy-Riemann equations. Discretized, this amounts to minimizing

.. math :: 
    E_C(a,b) = \begin{pmatrix} a^T & b^T\end{pmatrix}\begin{pmatrix}A & U \\ U^T & A\end{pmatrix}\begin{pmatrix} a \\ b\end{pmatrix}
    
for fixed :math:`a`, where :math:`U` is defined by

.. math ::
    a^T U b = \frac{1}{2}\sum_{ij \in \partial M} a_jb_i -a_i b_j.

See :cite:`CdGDSchroder13` for details.
Differentiating :math:`E_C(a,b)` with respect to :math:`b` yields the equation

.. math ::
    Ab + U^T a = 0,
    
which can then be solved for :math:`b`. 

While this yields a flattening :math:`f\colon V \to \mathbb C` that is "as conformal as possible", the exterior angles :math:`\tilde k` are not necessarily realised. To realise them precisely, one can alternatively interpolate :math:`b` harmonically just as :math:`a`. This approach computes :math:`b` as the discretized solution of

.. math ::
    \Delta b = 0 \quad \text{with} \quad b|_{\partial M}=\text{Im}(\tilde \gamma).



.. bibliography:: biblio.bib
